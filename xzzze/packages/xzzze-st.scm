(define-module (xzzze packages xzzze-st)
  #:use-module (gnu packages base)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages cups)
  #:use-module (gnu packages fonts)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages gawk)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages image)
  #:use-module (gnu packages libbsd)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages mpd)
  #:use-module (gnu packages ncurses)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages web)
  #:use-module (gnu packages webkit)
  #:use-module (gnu packages xorg)
  #:use-module (guix build-system glib-or-gtk)
  #:use-module (guix build-system gnu)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (xzzze packages))

(define-public xzzze-st
  (package
    (name "xzzze-st")
    (version "0.8.4")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
          (url "https://gitlab.com/xzzze/st.git")
	  (commit "c53cfdacd7eb37c3c950f6314fb2686e2226e393")))
        (sha256 (base32 "1ys3cj6wp74yilr7wqyzyxrqxycj00294kxcjbk6w6sjkd9d421n"))))
    (build-system gnu-build-system)
    (arguments
     `(#:tests? #f                      ; no tests
       #:make-flags
       (list (string-append "CC=" ,"gcc")
             (string-append "PREFIX=" %output))
       #:phases
       (modify-phases %standard-phases
         (delete 'configure)
         (add-after 'unpack 'inhibit-terminfo-install
           (lambda _
             (substitute* "Makefile"
               (("\ttic .*") ""))
             #t)))))
    (inputs
     `(("libx11" ,libx11)
       ("libxft" ,libxft)
       ("harfbuzz" ,harfbuzz)
       ("fontconfig" ,fontconfig)
       ("freetype" ,freetype)))
    (native-inputs
     `(("pkg-config" ,pkg-config)))
    (description "xzzze customized st build")
    (license license:x11)
    (home-page "https://gitlab.com/xzzze/st")
    (synopsis "xzzze's simple terminal build")))


