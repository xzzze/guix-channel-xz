(define-module (xzzze packages xzzze-dwm)
  #:use-module (gnu packages base)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages cups)
  #:use-module (gnu packages fonts)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages gawk)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages image)
  #:use-module (gnu packages libbsd)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages mpd)
  #:use-module (gnu packages ncurses)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages web)
  #:use-module (gnu packages webkit)
  #:use-module (gnu packages xorg)
  #:use-module (guix build-system glib-or-gtk)
  #:use-module (guix build-system gnu)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (xzzze packages))

(define-public xzzze-dwm
  (package
    (name "xzzze-dwm")
    (version "6.2")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
          (url "https://gitlab.com/xzzze/dwm.git")
          (commit "66c2b3fe124b031156fab771398c3016408f1a01")))
      (sha256 (base32 "06f8gcqhs9rznwvsm22d3hnv7aha724lc0q5dslk0dw39g62rm1a"))))
    (build-system gnu-build-system)
    (arguments
      `(#:tests? #f
		 #:make-flags (list (string-append "YAJLINC="
						   (assoc-ref %build-inputs "libyajl")
						   "/include/yajl")
				     (string-append "FREETYPEINC="
						    (assoc-ref %build-inputs "freetype")
						    "/include/freetype2"))
       #:phases
       (modify-phases %standard-phases
         (replace 'configure
           (lambda _
             (substitute* "Makefile" (("\\$\\{CC\\}") "gcc"))
             #t))
        (replace 'install
          (lambda* (#:key outputs #:allow-other-keys)
            (let ((out (assoc-ref outputs "out")))
              (invoke "make" "install"
                      (string-append "DESTDIR=" out) "PREFIX="))))
        (add-after 'build 'install-xsession
          (lambda* (#:key outputs #:allow-other-keys)
            ;; Add a .desktop file to xsessions.
            (let* ((output (assoc-ref outputs "out"))
                   (xsessions (string-append output "/share/xsessions")))
              (mkdir-p xsessions)
              (with-output-to-file
                  (string-append xsessions "/dwm.desktop")
                (lambda _
                  (format #t
                    "[Desktop Entry]~@
                     Name=dwm~@
                     Comment=Dynamic Window Manager~@
                     Exec=~a/bin/dwm~@
                     TryExec=~@*~a/bin/dwm~@
                     Icon=~@
                     Type=Application~%"
                    output)))
              #t))))))

    (inputs
      `(("libyajl" ,libyajl)
	("freetype" ,freetype)
	("libx11" ,libx11)
	("libxft" ,libxft)
	("libxinerama" ,libxinerama)))
    (home-page "https://gitlab.com/xzzze/dwm")
    (synopsis "xzzze dwm version")
    (description "dwm build with a few minimal patches applied")
    (license license:x11)))


