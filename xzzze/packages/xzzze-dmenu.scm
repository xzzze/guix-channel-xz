(define-module (xzzze packages xzzze-dmenu)
  #:use-module (gnu packages suckless)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (xzzze packages))

(define-public xzzze-dmenu
 (package
    (inherit dmenu)
    (name "xzzze-dmenu")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
          (url "https://gitlab.com/xzzze/dmenu.git")
          (commit "92659f6513f51f63fdb65d4fb17912db230f6382")))
        (sha256 (base32 "074c2cdjbnp307d869217rbw6qnmkm9cdvvk4sijlhamd7nvv11f"))))
    (home-page "https://gitlab.com/xzzze/dmenu")
    (synopsis "xzzze dmenu version")))

